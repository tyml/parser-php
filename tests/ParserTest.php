<?php

class AstVisitor extends \Tyml\Ast\AstVisitor
{
    protected function visitPrimitive(\Tyml\Ast\TymlPrimitive $node)
    {
        return $node->getValue();
    }
    
    protected function visitString(\Tyml\Ast\TymlString $node)
    {
        return $node->getValue();
    }
    
    protected function visitArray(\Tyml\Ast\TymlArray $node)
    {
        $result = array();
        foreach ($node->getItems() as $item)
            $result[] = $this->accept($item);
        return $result;
    }
   
    protected function visitObject(\Tyml\Ast\TymlObject $node)
    {
        $result = new \stdClass();
        foreach ($node->getAttributes() as $attr)
        {
            $attrName = $attr->getIdentifier()->getName();
            $result->$attrName = $this->accept($attr->getValue());
        }
        
        $result->type = $node->getTypeIdentifier()->getName();
        
        $imp = array();
        foreach ($node->getImplicitAttributes() as $implicit)
        {
            $imp[] = $this->accept($implicit);
        }
        
        $result->implicitAttributes = $imp;
        
        return $result;
    }
    
    protected function visitDocument(\Tyml\Ast\TymlDocument $node)
    {
        return $this->accept($node->getRootNode());
    }
}

class ParserTest extends PHPUnit_Framework_TestCase
{
    public function testParser1()
    {
        $parser = new \Tyml\Parser\Parser();
        $result = $parser->parse(<<<TYML
{!tyml 1.0}                
{test
    foo: <bar> bla:<test>
}
TYML
);
        
        print_r($result->getMessages());
        
        $visitor = new AstVisitor();
        $result = $visitor->accept($result->getDocument());
        print_r($result);
        
    }
}
