<?php

require_once "../vendor/autoload.php";

class AstVisitor extends \Tyml\Ast\AstVisitor
{
    protected function visitPrimitive(\Tyml\Ast\TymlPrimitive $node)
    {
        return $node->getValue();
    }
    
    protected function visitString(\Tyml\Ast\TymlString $node)
    {
        return $node->getValue();
    }
    
    protected function visitArray(\Tyml\Ast\TymlArray $node)
    {
        $result = array();
        foreach ($node->getItems() as $item)
            $result[] = $this->accept($item);
        return $result;
    }
   
    protected function visitObject(\Tyml\Ast\TymlObject $node)
    {
        $result = new \stdClass();
        foreach ($node->getAttributes() as $attr)
        {
            $attrName = $attr->getIdentifier()->getName();
            $result->$attrName = $this->accept($attr->getValue());
        }
        
        $result->type = $node->getTypeIdentifier()->getName();
        
        $imp = array();
        foreach ($node->getImplicitAttributes() as $implicit)
        {
            $imp[] = $this->accept($implicit);
        }
        
        $result->implicitAttributes = $imp;
        
        return $result;
    }
    
    protected function visitDocument(\Tyml\Ast\TymlDocument $node)
    {
        return $this->accept($node->getRootNode());
    }
}

$input = @$_POST["txt"];

$output = "";
$log = "";
if ($input !== null)
{
        $parser = new \Tyml\Parser\Parser();
        $result = $parser->parse($input);
        
        $log = print_r($result->getMessages(), true);
        
        $visitor = new AstVisitor();
        $result = $visitor->accept($result->getDocument());
        $output = print_r($result, true);
}

?>
<html>
    <body>
        Log:
        <pre>
        <?php echo $log; ?>
        </pre>
        
        Content:
        <pre>
        <?php echo $output; ?>
        </pre>
        <form method="post">
            <textarea name="txt" style="width: 70em; height: 20em;"><?php echo $input; ?></textarea>
            <input type="submit" name="submit" value="Ok" />
        </form>
    </body>
</html>