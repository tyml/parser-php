<?php

namespace Tyml\Parser;

class MessageDictionary
{
    public function getUnexpectedCharactersBeforeDocumentMessage($unexpectedChars)
    {
        return "Unexpected characters";
    }

    public function getNoDocumentHeaderMessage()
    {
        return "No document header found";
    }

    public function getEndOfDocumentExpectedMessage()
    {
        return "End of document expected";
    }

    public function getExpectedStringNotFoundMessage($expected)
    {
        return "Expected '" . $expected . "'";
    }


    public function getUnexpectedColonOrMissingAttributeNameMessage()
    {
        return "Unexpected ':' or missing attribute name";
    }

    public function getExpectedWhitespaceNotFoundMessage($lastElementType, $currentElementType)
    {
        if ($lastElementType === $currentElementType)
            return "Whitespace expected between two " . $currentElementType . "s";
        else
            return "Whitespace expected between " . $lastElementType . " and " . $currentElementType;
    }

    public function getTypeIdentifierCannotBeEmptyMessage()
    {
        return "Type identifier cannot be empty";
    }

    public function getTypeIdentifierCannotStartWithMessage($cannotStartWith)
    {
        return "Type identifier cannot start with '" . $cannotStartWith . "'";
    }

    public function getOnlyOnePrefixIsAllowedInTypeIdentifierMessage()
    {
        return "Only one prefix is allowed in type identifier";
    }

    public function getUnexpectedCharacterInPrimitiveMessage($character)
    {
        return "Unexpected character '" . $character . "' in primitive";
    }

    public function getUnexpectedCharacterInMarkupArrayMessage($character)
    {
        return "Unexpected character '" . $character . "' in markup array";
    }

    public function getUnclosedObjectMessage()
    {
        return "Object not closed, no matching '}'";
    }

    public function getUnclosedMarkupArrayMessage()
    {
        return "Markup array not closed, no matching ']'";
    }

    public function getUnclosedArrayMessage()
    {
        return "Array not closed, no matching ']'";
    }

    public function getExpectedHexadecimalDigitInUnicodeEscapeSequenceNotFoundMessage($gotActual)
    {
        return "Expected hexadecimal digit, but got '" . $gotActual . "'";
    }

    public function getUnrecognizedEscapeSequenceMessage($escapeSequence)
    {
        return "Unrecognized escape sequence'" . $escapeSequence . "'";
    }

    public function getInvalidEscapedStringIdentifierMessage($unexpectedCharacter)
    {
        return "Invalid escaped string identifier, unexpected character '" . $unexpectedCharacter . "'";
    }
}