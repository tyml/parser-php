<?php

namespace Tyml\Parser;

interface ParserInterface {
    /**
     * @param string $text
     * @return ParseResult
     */
    function parse($text);
} 