<?php

namespace Tyml\Parser;

class MessageType
{
    private $type;
    
    private function __construct($type)
    {
        $this->type = $type;
    }
    
    public static function fatal()
    {
        return new MessageType("FATAL");
    }
    
    public static function error()
    {
        return new MessageType("ERROR");
    }
    
    public static function warning()
    {
        return new MessageType("WARNING");
    }
    
    public static function info()
    {
        return new MessageType("INFO");
    }
}