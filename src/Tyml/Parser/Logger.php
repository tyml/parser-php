<?php

namespace Tyml\Parser;


use Tyml\TextRegion;

class Logger
{
    /**
     * @var Message[]
     */
    private $messages = array();

    private $messageDictionary;

    /**
     * @param MessageDictionary $messageDictionary
     */
    public function __construct(MessageDictionary $messageDictionary)
    {
        $this->messageDictionary = $messageDictionary;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    private function logError($code, $message, TextRegion $textRegion = null)
    {
        $this->messages[] = new Message(MessageType::error(),
            "TymlError" . $code, array(), $message . ".", $textRegion);
    }

    public function logExpectedTokensNotFound($expected, $region)
    {
        $this->logError(100, $this->messageDictionary->getExpectedStringNotFoundMessage($expected), $region);
    }


    public function logUnexpectedCharactersBeforeDocument($unexpectedChars, $region)
    {
        $this->logError(210,
            $this->messageDictionary->getUnexpectedCharactersBeforeDocumentMessage($unexpectedChars), $region);
    }

    public function logNoDocumentHeader()
    {
        $this->logError(220, $this->messageDictionary->getNoDocumentHeaderMessage());
    }

    public function logEndOfDocumentExpected($region)
    {
        $this->logError(230, $this->messageDictionary->getEndOfDocumentExpectedMessage(), $region);
    }


    public function logUnexpectedCharacterInPrimitive($character, $region)
    {
        $this->logError(300,
            $this->messageDictionary->getUnexpectedCharacterInPrimitiveMessage($character), $region);
    }


    public function logUnrecognizedEscapeSequence($escapeSequence, $region)
    {
        $this->logError(410,
            $this->messageDictionary->getUnrecognizedEscapeSequenceMessage($escapeSequence), $region);
    }

    public function logExpectedHexadecimalDigitInUnicodeEscapeSequenceNotFound($gotActual, $region)
    {
        $this->logError(420,
            $this->messageDictionary->getExpectedHexadecimalDigitInUnicodeEscapeSequenceNotFoundMessage($gotActual), $region);
    }

    public function logInvalidEscapedStringIdentifier($unexpectedCharacter, $region)
    {
        $this->logError(500,
            $this->messageDictionary->getInvalidEscapedStringIdentifierMessage($unexpectedCharacter), $region);
    }

    public function logTypeIdentifierCannotBeEmpty($region)
    {
        $this->logError(611, $this->messageDictionary->getTypeIdentifierCannotBeEmptyMessage(), $region);
    }

    public function logTypeIdentifierCannotStartWith($cannotStartWith, $region)
    {
        $this->logError(612,
            $this->messageDictionary->getTypeIdentifierCannotStartWithMessage($cannotStartWith), $region);
    }

    public function logOnlyOnePrefixIsAllowedInTypeIdentifier($region)
    {
        $this->logError(615, $this->messageDictionary->getOnlyOnePrefixIsAllowedInTypeIdentifierMessage(), $region);
    }

    public function logUnexpectedColonOrMissingAttributeName($region)
    {
        $this->logError(620, $this->messageDictionary->getUnexpectedColonOrMissingAttributeNameMessage(), $region);
    }

    public function logExpectedWhitespaceNotFoundInObject($lastElementType,
                                                          $currentElementType, $whitespaceErrorRegion)
    {
        $this->logError(630, $this->messageDictionary->getExpectedWhitespaceNotFoundMessage(
            $lastElementType, $currentElementType), $whitespaceErrorRegion);
    }

    public function logUnclosedObject($region)
    {
        $this->logError(640, $this->messageDictionary->getUnclosedObjectMessage(), $region);
    }

    public function logUnclosedArray($region)
    {
        $this->logError(700, $this->messageDictionary->getUnclosedArrayMessage(), $region);
    }

    public function logUnexpectedCharacterInMarkupArray($character, $region)
    {
        $this->logError(810, $this->messageDictionary->getUnexpectedCharacterInMarkupArrayMessage($character), $region);
    }

    public function logUnclosedMarkupArray($region)
    {
        $this->logError(820, $this->messageDictionary->getUnclosedMarkupArrayMessage(), $region);
    }
} 