<?php

namespace Tyml\Parser;

use Contruder\Common\StringHelper;
use Tyml\Ast\Implementation\TymlArrayExplicitImplementation;
use Tyml\Ast\Implementation\TymlArrayMarkupImplementation;
use Tyml\Ast\Implementation\TymlDocumentHeaderImplementation;
use Tyml\Ast\Implementation\TymlDocumentImplementation;
use Tyml\Ast\Implementation\TymlObjectAttributeIdentifierImplementation;
use Tyml\Ast\Implementation\TymlObjectAttributeImplementation;
use Tyml\Ast\Implementation\TymlObjectImplementation;
use Tyml\Ast\Implementation\TymlObjectNsAttributeImplementation;
use Tyml\Ast\Implementation\TymlObjectTypeIdentifierImplementation;
use Tyml\Ast\Implementation\TymlPrimitiveImplementation;
use Tyml\Ast\Implementation\TymlStringEscapedIdentifierBeginImplementation;
use Tyml\Ast\Implementation\TymlStringEscapedIdentifierEndImplementation;
use Tyml\Ast\Implementation\TymlStringEscapedImplementation;
use Tyml\Ast\Implementation\TymlStringImplicitImplementation;
use Tyml\Ast\Implementation\TymlStringNormalImplementation;
use Tyml\Internal\Expect;
use Tyml\Parser\Logger;
use Tyml\Parser\MessageDictionary;
use Tyml\Parser\Tokenizer;

class Parser
{
    private $letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private $numbers = "0123456789";

    /**
     * @var MessageDictionary
     */
    private $messageDictionary;

    private function isLetterIn($str, $letters)
    {
        if (strlen($str) !== 1)
            return false;

        return StringHelper::contains($letters, $str);
    }

    private function isValidIdentifierFirstLetter($letter)
    {
        return $this->isLetterIn($letter, $this->letters . "_");
    }

    private function isValidIdentifierOtherLetter($letter)
    {
        return $this->isLetterIn($letter, $this->letters . $this->numbers . "._-");
    }

    private function isValidPrimitiveLetter($letter)
    {
        return $this->isLetterIn($letter, $this->letters . $this->numbers . "_-+*=|~!?.,;/\"'()^&@%$#");
    }

    private function isWhitespaceChar($letter)
    {
        return $this->isLetterIn($letter, " \n\r\t");
    }

    private function getNotEqualToCondition($notEqualTo)
    {
        return function ($letter) use ($notEqualTo) {
            return $letter !== $notEqualTo;
        };
    }

    public function __construct(MessageDictionary $messageDictionary = null)
    {
        if ($messageDictionary === null)
            $this->messageDictionary = new MessageDictionary();
        else
            $this->messageDictionary = $messageDictionary;
    }


    /**
     * Parses $text.
     * 
     * @param string $text
     * @return ParseResult
     */
    public function parse($text)
    {
        Expect::that($text)->isString();

        $t = new Tokenizer($text);
        $logger = new Logger($this->messageDictionary);

        $doc = $this->parseDocument($t, $logger);

        return new ParseResult($doc, $logger->getMessages());
    }

    private function parseDocument(Tokenizer $t, Logger $logger)
    {
        $startPos = $t->getPosition();

        $invalidChars = $t->readWhile($this->getNotEqualToCondition("{"));
        if ($invalidChars !== "") {
            $logger->logUnexpectedCharactersBeforeDocument($invalidChars, $t->getRegion($startPos));
        }

        $header = $this->tryReadDocumentHeader($t, $logger);
        if ($header === null) {
            $logger->logNoDocumentHeader();
            $header = new TymlDocumentHeaderImplementation(new TymlPrimitiveImplementation("1.0"));
        }

        $this->parseWhitespace($t, $logger);
        $obj = $this->parseObject($t, $logger);
        $this->parseWhitespace($t, $logger);

        $pos = $t->getPosition();
        $invalidChars = $t->readToEnd();
        if ($invalidChars !== "") {
            $logger->logEndOfDocumentExpected($t->getRegion($pos));
        }

        return new TymlDocumentImplementation($header, $obj, $t->getRegion($startPos));
    }

    private function tryReadDocumentHeader(Tokenizer $t, Logger $logger)
    {
        $startPos = $t->getPosition();

        if (!$t->tryRead("{!tyml"))
            return null;

        $this->parseWhitespace($t, $logger);

        $versionElement = $this->parsePrimitive($t, $logger);

        $this->expect($t, $logger, "}");

        return new TymlDocumentHeaderImplementation($versionElement, null, $t->getRegion($startPos));
    }

    private function tryReadComment(Tokenizer $t, Logger $logger)
    {
        if ($t->tryRead("{--")) {
            $equalToWhitespaceCondition = function ($letter) {
                return $letter == " ";
            };
            $t->readWhile($equalToWhitespaceCondition);

            if ($t->peek() === "<") {
                $this->parseString($t, $logger);
                $t->readWhile($equalToWhitespaceCondition);
                $this->expect($t, $logger, "--}");
            } else
                $t->readWhile(function($c) use ($t) { return !$t->tryRead("--}"); });

            return true;
        }

        return null;
    }

    private function parseWhitespace(Tokenizer $t, Logger $logger)
    {
        $whitespace = "";

        while (true) {
            $nextChar = $t->peek();

            if ($nextChar === "{" && $this->tryReadComment($t, $logger)) {
                $whitespace .= " ";
                continue;
            }

            if (!$this->isWhitespaceChar($nextChar))
                break;

            $t->read();
            $whitespace .= $nextChar;
        }

        if ($whitespace === "")
            return null;

        return true;
    }

    /**
     * @param Tokenizer $t
     * @param Logger $logger
     * @param string $expected
     * @return bool
     */
    private function expect(Tokenizer $t, Logger $logger, $expected)
    {
        if (!$t->tryRead($expected)) {
            $pos = $t->getPosition();
            $logger->logExpectedTokensNotFound($expected, $t->getRegion($pos));
            return false;
        }

        return true;
    }

    private function parseObject(Tokenizer $t, Logger $logger)
    {
        $startPos = $t->getPosition();

        $this->expect($t, $logger, "{");

        $openingBracket = $t->getRegion($startPos);

        $identifier = $this->parseTypeIdentifier($t, $logger);

        $implicitAttributes = array();
        $attributes = array();
        $namespacePrefixDefinitions = array();

        $this->parseInnerObject($t, $logger, $implicitAttributes, $attributes, $namespacePrefixDefinitions);

        if (!$t->tryRead("}")) {
            $logger->logUnclosedObject($openingBracket);
        }

        return new TymlObjectImplementation($identifier,
            $attributes, $implicitAttributes, $namespacePrefixDefinitions, $t->getRegion($startPos));
    }

    private function parseInnerObject(Tokenizer $t, Logger $logger, array &$implicitAttributes,
                                      array &$attributes, array &$namespacePrefixDefinitions)
    {
        $lastElementType = "type";

        while (true) {
            $whitespace = $this->parseWhitespace($t, $logger);

            $nextChar = $t->peek();

            if ($nextChar === "}" || $nextChar === "")
                break;

            if ($nextChar === ":") {
                $p = $t->getPosition();
                $t->read();
                $logger->logUnexpectedColonOrMissingAttributeName($t->getRegion($p));
                continue;
            }

            $logWhitespaceError = false;
            $whitespaceErrorRegion = null;

            if ($whitespace === null) {
                $logWhitespaceError = true;
                $whitespaceErrorRegion = $t->getRegion($t->getPosition());
            }

            $nsAttr = $this->tryParseNamespacePrefixDefinition($t, $logger);

            if ($nsAttr !== null) {
                $namespacePrefixDefinitions[] = $nsAttr;
                continue;
            }

            $attrStartPos = $t->getPosition();
            $attrIdentifier = $this->tryParseAttributeIdentifier($t, $logger);
            if ($attrIdentifier !== null) {
                $t->read(); //read ":"

                $this->parseWhitespace($t, $logger);
            }

            $element = $this->parseElement($t, $logger);

            $currentElementType = null;

            if ($attrIdentifier === null) {
                $implicitAttributes[] = $element;
                $currentElementType = "element";
            } else {
                $attribute = new TymlObjectAttributeImplementation($attrIdentifier, $element, $t->getRegion($attrStartPos));
                $attributes[] = $attribute;
                $currentElementType = "attribute";
            }

            if ($logWhitespaceError) {
                $logger->logExpectedWhitespaceNotFoundInObject($lastElementType, $currentElementType, $whitespaceErrorRegion);
            }

            $lastElementType = $currentElementType;
        }

        $this->parseWhitespace($t, $logger);
    }

    private function parseTypeIdentifier(Tokenizer $t, Logger $logger)
    {
        $startPos = $t->getPosition();

        if ($t->peek() === "?") {
            $t->read();
            return new TymlObjectTypeIdentifierImplementation("", "?", $t->getRegion($startPos));
        }

        $identifier = "";
        $prefix = null;

        while (true) {
            $c = $t->peek();

            if ($identifier === "" && !$this->isValidIdentifierFirstLetter($c)) {
                if ($this->isWhitespaceChar($c))
                    $logger->logTypeIdentifierCannotBeEmpty($t->getRegion($startPos));
                else
                    $logger->logTypeIdentifierCannotStartWith($c, $t->getRegion($startPos));

                return new TymlObjectTypeIdentifierImplementation("", "?", $t->getRegion($startPos));
            }

            if ($c === "/") {
                if ($prefix !== null) {
                    $p = $t->getPosition();
                    $t->read();
                    $logger->logOnlyOnePrefixIsAllowedInTypeIdentifier($t->getRegion($p));
                    continue;
                } else {
                    $prefix = $identifier;
                    $identifier = "";
                }
                $t->read();
            } else if (!$this->isValidIdentifierOtherLetter($c)) {
                break;
            } else
                $identifier .= $t->read();
        }

        if ($prefix === null)
            $prefix = "";

        return new TymlObjectTypeIdentifierImplementation($prefix, $identifier, $t->getRegion($startPos));
    }


    /**
     * Tries to parse an attribute identifier.
     * An attribute identifier has to end with ":", however, ":" will not be read.
     * If it fails, the reader will be reset to the state before this method call.
     * If it succeeds, a TymlAttributeIdentifier will be returned.
     */
    private function tryParseAttributeIdentifier(Tokenizer $t, Logger $logger)
    {
        $startPos = $t->getPosition();

        $identifierStr = "";
        $prefix = null;

        while (true) {
            $c = $t->peek();

            if ($identifierStr === "" && !$this->isValidIdentifierFirstLetter($c)) {
                $t->gotoPosition($startPos);
                return null;
            }

            if ($c === "/") {
                if ($prefix !== null) {
                    $t->gotoPosition($startPos);
                    return null;
                } else {
                    $prefix = $identifierStr;
                    $identifierStr = "";
                }
                $t->read();
            } else if (!$this->isValidIdentifierOtherLetter($c)) {
                break;
            } else
                $identifierStr .= $t->read();
        }

        if ($prefix === null)
            $prefix = "";

        if ($identifierStr === "" || $t->peek() !== ":") {
            $t->gotoPosition($startPos);
            return null;
        }

        return new TymlObjectAttributeIdentifierImplementation($prefix, $identifierStr, $t->getRegion($startPos));
    }


    private function tryParseNamespacePrefixDefinition(Tokenizer $t, Logger $logger)
    {
        $startPos = $t->getPosition();

        if (!$t->tryRead("!ns"))
            return null;

        $identifier = "";

        if ($t->peek() === "/") {
            $t->read();

            while (true) {
                $c = $t->peek();

                if ($identifier === "" && !$this->isValidIdentifierFirstLetter($c)) {
                    $t->gotoPosition($startPos);
                    return null;
                }
                if (!$this->isValidIdentifierOtherLetter($c)) {
                    break;
                }

                $identifier .= $t->read();
            }
        }

        if (!$t->tryRead(":")) {
            $t->gotoPosition($startPos);
            return null;
        }

        $this->parseWhitespace($t, $logger);

        $ns = $this->parseString($t, $logger);

        return new TymlObjectNsAttributeImplementation($identifier, $ns, $t->getRegion($startPos));
    }


    private function parseElement(Tokenizer $t, Logger $logger)
    {
        $nextChar = $t->peek();
        $startPos = $t->getPosition();

        if ($nextChar === "!") {
            $t->read();
            $nextChar = $t->peek();

            if ($nextChar === "[")
                return $this->parseMarkupArray($t, $logger);
            else {
                $t->gotoPosition($startPos);
            }
        }

        if ($nextChar === "<")
            return $this->parseString($t, $logger);
        if ($nextChar === "[")
            return $this->parseArray($t, $logger);
        if ($nextChar === "{")
            return $this->parseObject($t, $logger);

        return $this->parsePrimitive($t, $logger);
    }

    private function parsePrimitive(Tokenizer $t, Logger $logger)
    {
        $startPos = $t->getPosition();

        $value = "";
        while ($this->isValidPrimitiveLetter($t->peek())) {
            $nextChar = $t->read();
            $value .= $nextChar;
        }

        if ($value === "") {
            $c = $t->read();
            $logger->logUnexpectedCharacterInPrimitive($c, $t->getRegion($startPos));
        }

        return new TymlPrimitiveImplementation($value, $t->getRegion($startPos));
    }

    private function parseMarkupArray(Tokenizer $t, Logger $logger)
    {

        $startPos = $t->getPosition();

        $this->expect($t, $logger, "[");

        $openingBracket = $t->getRegion($startPos);

        $elements = array();

        $str = "";
        $lastPos = $t->getPosition();

        while (true) {
            $nextChar = $t->peek();

            if ($nextChar === "\\") {
                $str .= $this->readEscapeSequence($t, $logger);
                continue;
            }

            if ($nextChar === "]" || $nextChar === "")
                break;

            if ($nextChar === "{" && $this->tryReadComment($t, $logger))
                continue;

            if ($nextChar === "{" || $nextChar === "<") {
                if ($str !== "") {
                    $elements[] = new TymlStringImplicitImplementation($str, $t->getRegion($lastPos));
                    $str = "";
                }

                $elements[] = $this->parseElement($t, $logger);
                $lastPos = $t->getPosition();
            } else {
                $pos = $t->getPosition();

                $t->read();

                if ($nextChar === "}" || $nextChar === "]" || $nextChar === "[") {
                    $logger->logUnexpectedCharacterInMarkupArray($nextChar, $t->getRegion($pos));
                } else
                    $str .= $nextChar;
            }
        }

        if ($str !== "") {
            $elements[] = new TymlStringImplicitImplementation($str, $t->getRegion($lastPos));
        }


        if (!$this->expect($t, $logger, "]")) {
            $logger->logUnclosedMarkupArray($openingBracket);
        }

        return new TymlArrayMarkupImplementation($elements, $t->getRegion($startPos));
    }


    private function parseArray(Tokenizer $t, Logger $logger)
    {

        $startPos = $t->getPosition();

        $this->expect($t, $logger, "[");

        $openingBracket = $t->getRegion($startPos);

        $elements = array();

        while (true) {
            $this->parseWhitespace($t, $logger);
            $next = $t->peek();
            if ($next === "]" || $next === "")
                break;

            $element = $this->parseElement($t, $logger);
            $elements[] = $element;
        }


        if (!$this->expect($t, $logger, "]")) {
            $logger->logUnclosedArray($openingBracket);
        }

        return new TymlArrayExplicitImplementation($elements, $t->getRegion($startPos));
    }

    //taken from http://stackoverflow.com/questions/9878483/php-equivlent-of-fromcharcode
    private function unichr($u)
    {
        return mb_convert_encoding('&#' . intval($u) . ';', 'UTF-8', 'HTML-ENTITIES');
    }

    private function readEscapeSequence(Tokenizer $t, Logger $logger)
    {
        $this->expect($t, $logger, "\\");

        $startPos = $t->getPosition();
        $next = $t->read();

        if ($next === "n")
            return "\n";
        else if ($next === "\\")
            return "\\";
        else if ($next === "t")
            return "\t";
        else if ($next !== "" && $this->isLetterIn($next, "<>[]{}"))
            return $next;
        else if ($next === "u") {

            $str = "";
            for ($i = 0; $i < 4; $i++) {
                $p = $t->getPosition();
                $c = $t->read();
                if (!$this->isLetterIn($next, $this->numbers . "ABCDEF")) {
                    $logger->logExpectedHexadecimalDigitInUnicodeEscapeSequenceNotFound($c, $t->getRegion($p));
                    break;
                }
                $str .= $c;
            }
            return $this->unichr(hexdec($str));
        }

        $logger->logUnrecognizedEscapeSequence("\\" . $next, $t->getRegion($startPos));
        return "\\" . $next;
    }

    /**
     * Parses a normal or escaped string.
     */
    private function parseString(Tokenizer $t, Logger $logger)
    {
        $startPos = $t->getPosition();

        $this->expect($t, $logger, "<");

        $str = "";
        $isValidIdentifier = true;
        $invalidChar = "";
        $startIdentifierStartPos = $t->getPosition();

        while (true) {
            $c = $t->peek();

            if ($c === "<" || $c === ">" || $c === "" || $c === "\n" || $c == "\r")
                break;

            if ($isValidIdentifier &&
                ($str === "" ? !$this->isValidIdentifierFirstLetter($c) : !$this->isValidIdentifierOtherLetter($c))
            ) {
                $isValidIdentifier = false;
                $invalidChar = $c;
            }

            if ($c === "\\") {
                $c = $this->readEscapeSequence($t, $logger);
            } else {
                $t->read();
            }

            $str .= $c;
        }

        if ($t->peek() === "<") {
            $startIdentifierRegion = $t->getRegion($startIdentifierStartPos);

            $t->read();
            $identifierStr = null;

            if (!$isValidIdentifier) {
                $logger->logInvalidEscapedStringIdentifier($invalidChar, $startIdentifierRegion);
                $identifierStr = "";
            } else
                $identifierStr = $str;

            $str = "";
            $endIdentifierRegion = null;

            while ($t->peek() !== "") {
                $endIdentifierPos = $t->getPosition();
                $subStr = $t->readWhile($this->getNotEqualToCondition(">"));

                $this->expect($t, $logger, ">");

                if ($str !== "") {
                    if ($subStr === $identifierStr) {
                        $endIdentifierRegion = $t->getRegion($endIdentifierPos);
                        break;
                    } else
                        $str .= ">";
                }
                $str .= $subStr;
            }

            if ($endIdentifierRegion === null)
                $endIdentifierRegion = $t->getRegion($t->getPosition());

            return new TymlStringEscapedImplementation($str,
                new TymlStringEscapedIdentifierBeginImplementation($identifierStr, $startIdentifierRegion),
                new TymlStringEscapedIdentifierEndImplementation($identifierStr, $endIdentifierRegion),
                $t->getRegion($startPos));
        }

        $this->expect($t, $logger, ">");

        return new TymlStringNormalImplementation($str, $t->getRegion($startPos));
    }
}
