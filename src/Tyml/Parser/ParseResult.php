<?php

namespace Tyml\Parser;

use Nunzion\Expect;
use Tyml\Ast\TymlDocument;

class ParseResult
{
    /**
     * @var TymlDocument
     */
    private $doc;
    /**
     *
     * @var Message[] 
     */
    private $messages;

    /**
     * 
     * @param TymlDocument $doc
     * @param Message[] $messages
     */
    public function __construct(TymlDocument $doc, array $messages)
    {
        Expect::that($messages)->isArrayOf(Message::getClassName());

        $this->doc = $doc;
        $this->messages = $messages;
    }
    
    /**
     * @return TymlDocument
     */
    public function getDocument()
    {
        return $this->doc;
    }
    
    /**
     * @return Message[]
     */
    public function getMessages()
    {
        return $this->messages;
    }
}