<?php

namespace Tyml\Parser;

use Nunzion\Expect;
use Tyml\Parser\MessageType;
use Tyml\TextRegion;

class Message
{
    public static function getClassName()
    {
        return get_called_class();
    }

    /**
     * @var MessageType
     */
    private $type;

    /**
     * @var array
     */
    private $arguments;

    /**
     * @var string
     */
    private $message;

    /**
     * @var TextRegion
     */
    private $textRegion;

    /**
     * @var string
     */
    private $messageId;

    public function __construct(MessageType $type, $messageId,
                                array $arguments, $message, TextRegion $textRegion = null)
    {
        Expect::that($messageId)->isString();
        Expect::that($message)->isString();

        $this->type = $type;
        $this->messageId = $messageId;
        $this->arguments = $arguments;
        $this->message = $message;
        $this->textRegion = $textRegion;
    }

    public function getType()
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return TextRegion|null
     *
     * The text region. Can be null to indicate a message which does not belong to a text block.
     */
    public function getTextRegion()
    {
        return $this->textRegion;
    }

    /**
     * The id of this message type. The set of message ids ever returned by the parser must be finite.
     *
     * @return string
     */
    public function getMessageId()
    {
        return $this->messageId;
    }
}