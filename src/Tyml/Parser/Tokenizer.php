<?php

namespace Tyml\Parser;

use Tyml\Internal\Expect;
use Tyml\TextRegion;

class Tokenizer
{
    private $text;
    private $currentPosition = 0;

    private $currentLine = 0;
    private $currentColumn = 0;

    /**
     * @param string $text
     */
    public function __construct($text)
    {
        Expect::that($text)->isString();

        $this->text = $text;
    }

    /**
     * @param string $expectedText
     * @return bool
     */
    public function tryRead($expectedText)
    {
        $pos = $this->getPosition();
        for ($i = 0; $i < strlen($expectedText); $i++) {
            if ($this->read() !== $expectedText[$i]) {
                $this->gotoPosition($pos);
                return false;
            }
        }

        return true;
    }

    public function peek()
    {
        if ($this->currentPosition >= strlen($this->text))
            return "";
        return $this->text[$this->currentPosition];
    }

    public function read()
    {
        if ($this->currentPosition >= strlen($this->text))
            return "";

        $result = $this->text[$this->currentPosition];
        $this->currentPosition++;

        if ($result === "\n") {
            $this->currentLine++;
            $this->currentColumn = 0;
        } else {
            $this->currentColumn++;
        }

        return $result;
    }

    public function getPosition()
    {
        return array(
            "position" => $this->currentPosition,
            "column" => $this->currentColumn,
            "line" => $this->currentLine
        );
    }

    public function gotoPosition($position)
    {
        $this->currentPosition = $position["position"];
        $this->currentColumn = $position["column"];
        $this->currentLine = $position["line"];
    }

    /**
     * @param callable $condition
     */
    public function readWhile($condition)
    {
        $chars = "";
        while (true) {
            $nextChar = $this->peek();
            if ($nextChar === "" || !$condition($nextChar))
                break;
            $this->read();
            $chars .= $nextChar;
        }

        return $chars;
    }

    public function readToEnd()
    {
        return $this->readWhile(function($l) { return true; });
    }

    /**
     * @param $startPos mixed
     * @return TextRegion
     */
    public function getRegion($startPos)
    {
        return new TextRegion($startPos["line"], $startPos["column"],
            $this->currentLine, $this->currentColumn);
    }
}