<?php

namespace Tyml\Ast;

interface TymlStringEscapedIdentifierEnd extends TymlNode
{
    /**
     * Gets the identifier.
     * 
     * @return string the identifier.
     */
    function getIdentifier();
}
