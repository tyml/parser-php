<?php

namespace Tyml\Ast;

interface TymlObjectNsAttribute extends TymlNode
{
    /**
     * Gets the prefix of the namespace attribute.
     * 
     * @return string the prefix.
     */
    function getPrefix();
    
    
    /**
     * Gets the namespace which will be associated to the prefix.
     * 
     * @return string the namespace.
     */
    function getNamespace();
}
