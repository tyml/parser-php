<?php

namespace Tyml\Ast;

interface TymlDocument extends TymlNode
{
    /**
     * Gets the header of the document.
     * 
     * @return TymlDocumentHeader the header.
     */
    function getHeader();
    
    /**
     * Gets the root node of the document.
     * 
     * @return TymlNode the root node.
     */
    function getRootNode();
}