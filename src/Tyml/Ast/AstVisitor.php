<?php

namespace Tyml\Ast;

abstract class AstVisitor
{
    /**
     * @param TymlNode $node
     * @param mixed $state
     * @return mixed
     */
    public function accept(TymlNode $node, $state = null)
    {
        if ($node instanceof TymlPrimitive)
            return $this->visitPrimitive($node, $state);
        else if ($node instanceof TymlObject)
            return $this->visitObject($node, $state);
        else if ($node instanceof TymlArrayMarkup)
            return $this->visitArrayMarkup($node, $state);
        else if ($node instanceof TymlArrayExplicit)
            return $this->visitArrayExplicit($node, $state);
        else if ($node instanceof TymlStringNormal)
            return $this->visitStringNormal($node, $state);
        else if ($node instanceof TymlStringEscaped)
            return $this->visitStringEscaped($node, $state);
        else if ($node instanceof TymlStringImplicit)
            return $this->visitStringImplicit($node, $state);
        else if ($node instanceof TymlDocument)
            return $this->visitDocument($node, $state);
        
        throw new \Exception("Element type is not supported!");
    }
    
    protected function visitNode(TymlNode $node, $state)
    {
        throw new \Exception("Not implemented");
    }
    
    protected function visitDocument(TymlDocument $node, $state)
    {
        return $this->visitNode($document, $state);
    }
    
    protected function visitElement(Element $node, $state)
    {
        return $this->visitNode($node, $state);
    }
    
    protected function visitObject(TymlObject $node, $state)
    {
        return $this->visitElement($node, $state);
    }
    
    protected function visitPrimitive(TymlPrimitive $node, $state)
    {
        return $this->visitElement($node, $state);
    }
    
    protected function visitArray(TymlArray $node, $state)
    {
        return $this->visitElement($node, $state);
    }
    
    protected function visitArrayExplicit(TymlArrayExplicit $node, $state)
    {
        return $this->visitArray($node, $state);
    }

    protected function visitArrayMarkup(TymlArrayMarkup $node, $state)
    {
        return $this->visitArray($node, $state);
    }
    
    protected function visitString(TymlString $node, $state)
    {
        return $this->visitElement($node, $state);
    }
    
    protected function visitStringNormal(TymlStringNormal $node, $state)
    {
        return $this->visitString($node, $state);
    }
    
    protected function visitStringImplicit(TymlStringImplicit $node, $state)
    {
        return $this->visitString($node, $state);
    }
    
    protected function visitStringEscaped(TymlStringEscaped $node, $state)
    {
        return $this->visitString($node, $state);
    }
}