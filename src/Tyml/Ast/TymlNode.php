<?php

namespace Tyml\Ast;

interface TymlNode
{

    /**
     * Gets the string representation of this node.
     *
     * @return string the string representation.
     */
    function __toString();

    /**
     * Resolves a prefix to its associated url.
     *
     * @param string prefix the prefix to resolve. 
     * Can be empty to resolve the current namespace. Cannot be null.
     * @return string the url associated to the prefix 
     * or the null string if the prefix does not exist.
     */
    function resolvePrefix($prefix);
}
