<?php

namespace Tyml\Ast;

interface TymlPrimitive extends TymlElement
{
    /**
     * Gets the value.
     * 
     * @return string the value.
     */    
    function getValue();
}