<?php

namespace Tyml\Ast;

interface TymlStringEscapedIdentifierBegin extends TymlNode
{
    /**
     * Gets the identifier.
     * 
     * @return string the identifier.
     */
    function getIdentifier();
}
