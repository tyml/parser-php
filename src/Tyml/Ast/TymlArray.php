<?php

namespace Tyml\Ast;

interface TymlArray extends TymlElement
{
    /**
     * Gets the items of the array.
     * 
     * @return TymlElement[]
     */
    function getItems();
}
