<?php

namespace Tyml\Ast;

interface TymlString extends TymlElement
{
    /**
     * Gets the string value.
     * 
     * @return string the value.
     */
    function getValue();
}
