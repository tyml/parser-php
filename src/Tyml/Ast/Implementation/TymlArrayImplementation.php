<?php

namespace Tyml\Ast\Implementation;

use Tyml\Internal\Expect;

abstract class TymlArrayImplementation extends TymlElementImplementation implements \Tyml\Ast\TymlArray
{
    private $items;
    
    /**
     * 
     * @param TymlElementImplementation[] $items
     * @param \Tyml\TextRegion $textRegion
     */
    public function __construct(array $items, \Tyml\TextRegion $textRegion = null)
    {
        parent::__construct($textRegion);
        
        Expect::that($items)->isArrayOf(TymlElementImplementation::getClassName());

        foreach ($items as $item)
        {
            $item->_setParent($this);
        }

        $this->items = $items;
    }
    
    /**
     * 
     * @return TymlElementImplementation[]
     */
    public function getItems()
    {
        return $this->items;
    }
}