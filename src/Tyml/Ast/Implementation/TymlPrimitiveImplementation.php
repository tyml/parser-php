<?php

namespace Tyml\Ast\Implementation;

use Tyml\Internal\Expect;

class TymlPrimitiveImplementation extends TymlElementImplementation implements \Tyml\Ast\TymlPrimitive
{
    private $value;

    /**
     * @param string $value
     * @param \Tyml\TextRegion $textRegion
     */
    public function __construct($value, \Tyml\TextRegion $textRegion = null)
    {
        parent::__construct($textRegion);
        
        Expect::that($value)->isString();
        
        $this->value = $value;
    }
    
    public function __toString()
    {
        return $this->value;
    }
    
    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
}