<?php

namespace Tyml\Ast\Implementation;

use Tyml\Internal\ArrayHelper;

class TymlArrayMarkupImplementation extends TymlArrayImplementation implements \Tyml\Ast\TymlArrayExplicit
{
    public function __toString()
    {
        return "![" . ArrayHelper::toString($this->getItems(), " ") . "]";
    }
}