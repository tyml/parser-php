<?php

namespace Tyml\Ast\Implementation;

use Tyml\Ast\TymlDocument;
use Tyml\TextRegion;

class TymlDocumentImplementation extends TymlNodeImplementation
    implements TymlDocument
{
    /**
     *
     * @var TymlDocumentHeaderImplementation
     */
    private $header;
    
    /**
     *
     * @var TymlObjectImplementation
     */
    private $root;
    
    public function __construct(TymlDocumentHeaderImplementation $header, 
            TymlObjectImplementation $root, TextRegion $textRegion = null)
    {
        parent::__construct($textRegion);
        
        $this->header = $header;
        $this->root = $root;
        $this->root->_setParent($this);
    }
    
    /**
     * @return TymlDocumentHeaderImplementation
     */
    public function getHeader()
    {
        return $this->header;
    }
    
    /**
     * @return TymlObjectImplementation
     */
    public function getRootNode()
    {
        return $this->root;
    }
    
    public function __toString()
    {
        return $this->root->__toString();
    }
}