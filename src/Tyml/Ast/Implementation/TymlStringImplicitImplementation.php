<?php

namespace Tyml\Ast\Implementation;

class TymlStringImplicitImplementation extends TymlStringImplementation implements \Tyml\Ast\TymlStringImplicit
{
    public function __toString()
    {
        return $this->getValue();
    }
}