<?php

namespace Tyml\Ast\Implementation;

class TymlStringNormalImplementation extends TymlStringImplementation implements \Tyml\Ast\TymlStringNormal
{

    public function __toString()
    {
        return "<" . $this->getValue() . ">";
    }

}
