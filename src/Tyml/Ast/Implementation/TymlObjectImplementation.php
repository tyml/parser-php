<?php

namespace Tyml\Ast\Implementation;

use Tyml\Internal\Expect;
use Tyml\Internal\ArrayHelper;

class TymlObjectImplementation extends TymlElementImplementation implements \Tyml\Ast\TymlObject
{
    private $typeIdentifier;
    private $attributes;
    private $implicitAttributes;
    
    /**
     *
     * @var TymlObjectNsAttributeImplementation[]
     */
    private $namespacePrefixDefinitions;
    
    /**
     * 
     * @param TymlObjectTypeIdentifierImplementation $typeIdentifier
     * @param TymlObjectAttributeImplementation[] $attributes
     * @param array $implicitAttributes
     * @param array $namespacePrefixDefinitions
     * @param \Tyml\TextRegion $textRegion
     */
    public function __construct(TymlObjectTypeIdentifierImplementation $typeIdentifier,
            array $attributes, array $implicitAttributes, array $namespacePrefixDefinitions, 
            \Tyml\TextRegion $textRegion)
    {
        parent::__construct($textRegion);
        
        Expect::that($attributes)->isArrayOf(
                TymlObjectAttributeImplementation::getClassName());
        Expect::that($implicitAttributes)->isArrayOf(
                TymlElementImplementation::getClassName());
        Expect::that($namespacePrefixDefinitions)->isArrayOf(
                TymlObjectNsAttributeImplementation::getClassName());

        $typeIdentifier->_setParent($this);
        foreach ($attributes as $item)
        {
            $item->_setParent($this);
        }
        foreach ($implicitAttributes as $item)
        {
            $item->_setParent($this);
        }
        foreach ($namespacePrefixDefinitions as $item)
        {
            $item->_setParent($this);
        }

        $this->typeIdentifier = $typeIdentifier;
        $this->attributes = $attributes;
        $this->implicitAttributes = $implicitAttributes;
        $this->namespacePrefixDefinitions = $namespacePrefixDefinitions;
    }
    
    public function __toString()
    {
        $att = ArrayHelper::toString($this->attributes, " ");
        if ($att != "")
            $att = " " . $att;
        $imp = ArrayHelper::toString($this->implicitAttributes, " ");
        if ($imp != "")
            $imp = " " . $imp;
        
        return "{" . $this->typeIdentifier->__toString() . $att . $imp . "}";
    }
    
    public function getTypeIdentifier()
    {
        return $this->typeIdentifier;
    }
    
    public function getAttributes()
    {
        return $this->attributes;
    }
    
    public function getImplicitAttributes()
    {
        return $this->implicitAttributes;
    }
    
    public function getNamespacePrefixDefinitions()
    {
        return $this->namespacePrefixDefinitions;
    }
    
    public function resolvePrefix($prefix)
    {
        Expect::that($prefix)->isString();
        
        foreach ($this->namespacePrefixDefinitions as $p)
        {
            if ($p->getPrefix() === $prefix)
                return $p->getNamespace()->getValue();
        }
        
        return parent::resolvePrefix($prefix);
    }
}