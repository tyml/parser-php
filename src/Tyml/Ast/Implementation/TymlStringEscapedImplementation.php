<?php

namespace Tyml\Ast\Implementation;

use Tyml\TextRegion;

class TymlStringEscapedImplementation extends TymlStringImplementation implements \Tyml\Ast\TymlStringEscaped
{

    private $beginIdentifier;
    private $endIdentifier;

    /**
     * @param string $value
     * @param TymlStringEscapedIdentifierBeginImplementation $beginIdentifier
     * @param TymlStringEscapedIdentifierEndImplementation $endIdentifier
     * @param TextRegion $textRegion
     */
    public function __construct($value, TymlStringEscapedIdentifierBeginImplementation $beginIdentifier,
                     TymlStringEscapedIdentifierEndImplementation $endIdentifier, TextRegion $textRegion = null)
    {
        parent::__construct($value, $textRegion);

        $beginIdentifier->_setParent($this);
        $endIdentifier->_setParent($this);

        $this->beginIdentifier = $beginIdentifier;
        $this->endIdentifier = $endIdentifier;
    }

    public function __toString()
    {
        return "<" . $this->getIdentifier() . "<" . $this->getValue() . ">" . $this->getIdentifier() . ">";
    }

    public function getBeginIdentifier()
    {
        return $this->beginIdentifier;
    }
    
    public function getEndIdentifier()
    {
        return $this->endIdentifier;
    }
    
    public function getIdentifier()
    {
        return $this->beginIdentifier->getIdentifier();
    }
}
