<?php

namespace Tyml\Ast\Implementation;

use Tyml\TextRegion;

abstract class TymlIdentifierImplementation extends TymlNodeImplementation
{
    /**
     * @var string
     */
    private $name;
    
    /**
     * @var string
     */
    private $prefix;

    /**
     * @param string $prefix
     * @param string $name
     * @param TextRegion $textRegion
     */
    public function __construct($prefix, $name, TextRegion $textRegion = null)
    {
        $this->name = $name;
        $this->prefix = $prefix;
    }
    
    public function __toString()
    {
        if ($this->getPrefix() === "")
            return $this->getName();
        
        return $this->prefix . "/" . $this->getName();
    }
    
    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }
    
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * @return string
     */
    public function getNamespace()
    {
        return $this->resolvePrefix($this->getPrefix());
    }
    
    /**
     * @return string
     */
    public function getFullQualifiedName()
    {
        $ns = $this->getNamespace();
        if ($ns === "")
            return $this->getName();
        else
            return $ns . "#" . $this->getName();
    }
}
