<?php

namespace Tyml\Ast\Implementation;

use Tyml\Internal\Expect;

class TymlObjectNsAttributeImplementation extends TymlNodeImplementation
{
    /**
     * @var string
     */
    private $prefix;
    
    /**
     * @var TymlStringImplementation
     */
    private $namespace;
    
    public function __construct($prefix, TymlStringImplementation $namespace)
    {
        Expect::that($prefix)->isString();

        $namespace->_setParent($this);

        $this->prefix = $prefix;
        $this->namespace = $namespace;
    }
    
    public function getPrefix()
    {
        return $this->prefix;
    }
    
    public function getNamespace()
    {
        return $this->namespace;
    }
    
    public function __toString()
    {
        return $this->prefix . ": " . $this->namespace->__toString();
    }
}
