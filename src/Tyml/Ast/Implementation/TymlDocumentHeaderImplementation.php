<?php

namespace Tyml\Ast\Implementation;

use Tyml\Ast\TymlDocumentHeader;
use Tyml\TextRegion;

class TymlDocumentHeaderImplementation extends TymlNodeImplementation implements TymlDocumentHeader
{

    private $versionElement;
    private $encoding;

    public function __construct(TymlPrimitiveImplementation $versionElement, 
            TymlStringImplementation $encoding = null, TextRegion $textRegion = null)
    {
        parent::__construct($textRegion);

        $versionElement->_setParent($this);
        if ($encoding != null)
            $encoding->_setParent($this);

        $this->versionElement = $versionElement;
        $this->encoding = $encoding;
    }

    public function __toString()
    {
        return "{!tyml}"; //todo
    }
    
    /**
     * @return TymlPrimitiveImplementation
     */
    public function getVersionElement()
    {
        return $this->versionElement;
    }
    
    /*
     * @return TymlStringImplementation 
     */
    public function getEncoding()
    {
        return $this->encoding;
    }
}
