<?php

namespace Tyml\Ast\Implementation;

use Tyml\Ast\TymlObjectAttribute;

class TymlObjectAttributeImplementation extends TymlNodeImplementation implements TymlObjectAttribute
{
    /**
     * @var TymlObjectAttributeIdentifierImplementation
     */
    private $identifier;
    
    /**
     * @var TymlElementImplementation
     */
    private $value;
    
    public function __construct(TymlObjectAttributeIdentifierImplementation $identifier,
            TymlElementImplementation $value, \Tyml\TextRegion $textRegion = null)
    {
        parent::__construct($textRegion);

        $identifier->_setParent($this);
        $value->_setParent($this);

        $this->identifier = $identifier;
        $this->value = $value;
    }
    
    public function getIdentifier()
    {
        return $this->identifier;
    }
    
    public function getValue()
    {
        return $this->value;
    }
    
    public function __toString()
    {
        return $this->identifier->__toString() . ": " . $this->value->__toString();
    }
}
