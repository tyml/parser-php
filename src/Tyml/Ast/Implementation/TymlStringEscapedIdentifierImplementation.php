<?php

namespace Tyml\Ast\Implementation;

use Tyml\Internal\Expect;

class TymlStringEscapedIdentifierImplementation
    extends TymlNodeImplementation
{
    private $identifier;

    /**
     * @param string $identifier
     * @param \Tyml\TextRegion $textRegion
     */
    public function __construct($identifier, \Tyml\TextRegion $textRegion = null)
    {
        parent::__construct($textRegion);
        
        Expect::that($identifier)->isString();
        $this->identifier = $identifier;
    }
    
    public function getIdentifier()
    {
        return $this->identifier;
    }
    
    public function __toString()
    {
        return $this->identifier;
    }
}