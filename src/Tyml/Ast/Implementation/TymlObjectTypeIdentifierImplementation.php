<?php

namespace Tyml\Ast\Implementation;

use Tyml\Ast\TymlObjectTypeIdentifier;

class TymlObjectTypeIdentifierImplementation extends TymlIdentifierImplementation
                                implements TymlObjectTypeIdentifier
{

}