<?php

namespace Tyml\Ast\Implementation;

class TymlObjectAttributeIdentifierImplementation extends TymlIdentifierImplementation 
                    implements \Tyml\Ast\TymlObjectAttributeIdentifier
{
    public function getNamespace()
    {
        if ($this->getPrefix() === "")
            return "";
        
        return parent::getNamespace();
    }
}