<?php

namespace Tyml\Ast\Implementation;

use LogicException;
use Tyml\Ast\Implementation\TymlNodeImplementation;
use Tyml\Ast\TymlNode;
use Tyml\Ast\TymlTextRegionNode;
use Tyml\Internal\Expect;
use Tyml\TextRegion;

abstract class TymlNodeImplementation implements TymlNode, TymlTextRegionNode
{
    public static function getClassName()
    {
        return get_called_class();
    }

    /**
     * @var TextRegion
     */
    private $textRegion;

    /**
     * @var TymlNodeImplementation
     */
    private $parent;

    private $cachedPrefixes = array();
    
    public function __construct(TextRegion $textRegion = null)
    {
        $this->textRegion = $textRegion;
    }

    /**
     * Gets the text region of this node.
     * 
     * @return TextRegion
     */
    public function getTextRegion()
    {
        return $this->textRegion;
    }

    public abstract function __toString();

    /**
     * Internal method. Will be called automatically 
     * on construction of the parent node.
     * 
     * @param TymlNodeImplementation $parent the parent.
     */
    public function _setParent(TymlNodeImplementation $parent)
    {
        if ($this->parent === null)
            $this->parent = $parent;
        else
            throw new LogicException("_setParent was already called.");
    }

    /**
     * Resolves a prefix to its associated uri.
     * 
     * @param string $prefix the prefix to resolve. 
     * Can be empty to resolve the current namespace. Cannot be null.
     * @return string the url associated to the prefix 
     * or the null string if the prefix does not exist.
     */
    public function resolvePrefix($prefix)
    {
        Expect::that($prefix)->isString();

        if (isset($this->cachedPrefixes[$prefix]))
            return $this->cachedPrefixes[$prefix];
        
        if ($this->parent === null)
        {
            if ($prefix === "")
                return "";
            return null;
        }
        
        $result = $this->parent->resolvePrefix($prefix);
        $this->cachedPrefixes[$prefix] = $result;
        return $result;
    }

}
