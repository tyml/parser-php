<?php

namespace Tyml\Ast\Implementation;

use Tyml\Internal\Expect;

abstract class TymlStringImplementation extends TymlElementImplementation implements \Tyml\Ast\TymlString
{
    private $value;
    
    /**
     * @param string $value
     */
    public function __construct($value, \Tyml\TextRegion $textRegion)
    {
        parent::__construct($textRegion);
        
        Expect::that($value)->isString();
        
        $this->value = $value;
    }
    
    /**
     * @return string
     */    
    public function getValue()
    {
       return $this->value; 
    }
}