<?php

namespace Tyml\Ast;

interface TymlObject extends TymlElement
{
    /**
     * Gets the type identifier.
     * 
     * @return TymlObjectTypeIdentifier the type identifier.
     */    
    function getTypeIdentifier();
    
    
    /**
     * Gets the attributes.
     * 
     * @return TymlObjectAttribute[] the attributes.
     */
    function getAttributes();
    
    
    /**
     * Gets the implicit attributes, i.e. attributes without a name.
     * 
     * @return TymlElement[] the implicit attributes.
     */
    function getImplicitAttributes();
    
    /**
     * Gets the namespace prefix definitions.
     * 
     * @return TymlObjectNsAttribute[] the namespace prefix definitions.
     */
    function getNamespacePrefixDefinitions();
}