<?php

namespace Tyml\Ast;

interface TymlObjectAttributeIdentifier extends TymlNode {
    
    /**
     * Gets the namespace of the type.
     * The namespace is the resolved prefix.
     * 
     * @return string the namespace.
     */
    function getNamespace();

    /**
     * Gets the prefix.
     * 
     * @return string the prefix.
     */
    function getPrefix();

    /**
     * Gets the name of the type without the prefix.
     * 
     * @return string the name.
     */
    function getName();

    /**
     * Gets the full qualified name of the type, 
     * including the namespace and the name.
     * 
     * @return string the full qualified name.
     */
    function getFullQualifiedName();
}