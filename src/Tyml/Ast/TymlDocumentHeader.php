<?php

namespace Tyml\Ast;

interface TymlDocumentHeader extends TymlNode
{
    /**
     * Gets the version of the document.
     * 
     * @return TymlPrimitive the version element.
     */
    function getVersionElement();
    
    /**
     * Gets the encoding of the document.
     * 
     * @return TymlString the encoding.
     */
    function getEncoding();
}