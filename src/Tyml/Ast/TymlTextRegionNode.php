<?php

namespace Tyml\Ast;

use Tyml\TextRegion;

interface TymlTextRegionNode extends TymlNode
{
    /**
     * Gets the text region of the node.
     * @return TextRegion
     */
    function getTextRegion();
}