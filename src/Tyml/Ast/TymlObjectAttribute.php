<?php

namespace Tyml\Ast;

interface TymlObjectAttribute extends TymlNode {
    
    /**
     * Gets the attribute identifier.
     * 
     * @return TymlObjectAttributeIdentifier the identifier.
     */
    function getIdentifier();
    
    
    /**
     * Gets the attribute value.
     * 
     * @return TymlElement the attribute value.
     */
    function getValue();
}