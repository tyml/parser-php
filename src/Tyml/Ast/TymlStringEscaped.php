<?php

namespace Tyml\Ast;

interface TymlStringEscaped extends TymlString
{
    /**
     * Gets the identifier.
     * 
     * @return string the identifier.
     */
    function getIdentifier();
    
    
    /**
     * Gets the begin identifier.
     * 
     * @return TymlStringEscapedIdentifierBegin
     */
    function getBeginIdentifier();
    
    /**
     * Gets the end identifier.
     * 
     * @return TymlStringEscapedIdentifierEnd
     */
    function getEndIdentifier();
}
