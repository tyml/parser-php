<?php

namespace Tyml;

use Tyml\Internal\Expect;

class TextRegion
{

    private $startLine;
    private $startColumn;
    private $endLine;
    private $endColumn;

    /**
     * Creates a new text region.
     * 
     * @param int $startLine the start line.
     * @param int $startColumn the start column.
     * @param int $endLine the end line.
     * @param int $endColumn the end column.
     */
    public function __construct($startLine, $startColumn, $endLine, $endColumn)
    {
        Expect::that($startLine)->isInt();
        Expect::that($startColumn)->isInt();
        Expect::that($endLine)->isInt();
        Expect::that($endColumn)->isInt();
        
        $this->startLine = $startLine;
        $this->startColumn = $startColumn;
        $this->endLine = $endLine;
        $this->endColumn = $endColumn;
    }
    
    public function getStartLine()
    {
        return $this->startLine;
    }

    public function getStartColumn()
    {
        return $this->startColumn;
    }

    public function getEndLine()
    {
        return $this->endLine;
    }

    public function getEndColumn()
    {
        return $this->endColumn;
    }

    public function __toString()
    {
        return \sprintf("%d: %d - %d: %d", $this->getStartLine(),
                $this->getStartColumn(), $this->getEndLine(),
                $this->getEndColumn());
    }
}
